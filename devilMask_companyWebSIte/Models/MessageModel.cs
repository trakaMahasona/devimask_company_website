﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace devilMask_companyWebSIte.Models
{
    public class MessageModel
    {

        [Required]
        [StringLength(20,MinimumLength =5)]
        public string FullName { get; set; }
        [Required]
        [StringLength(10,MinimumLength =10)]
        public Nullable<int> PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Message1 { get; set; }
        public int messege_id { get; set; }
    }
}